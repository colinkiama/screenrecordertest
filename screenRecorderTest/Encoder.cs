﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Composition;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Graphics;
using Windows.Graphics.Capture;
using Windows.Graphics.DirectX;
using Windows.Media.Core;
using Windows.Media.MediaProperties;
using Windows.Media.Transcoding;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Composition;

namespace screenRecorderTest
{
    public class Encoder
    {

        private CanvasDevice _device;
        private VideoStreamDescriptor _videoDescriptor;
        private MediaStreamSource _mediaStreamSource;
        private MediaTranscoder _transcoder;
        private bool _isRecording;
        private bool _closed = false;
        private uint _sourceWidth;
        private uint _sourceHeight;


        // Capture API objects.
        private SizeInt32 _lastSize;
        private GraphicsCaptureItem _item;
        private Direct3D11CaptureFramePool _framePool;
        private GraphicsCaptureSession _session;

        // Non-API related members.
        private CompositionGraphicsDevice _compositionGraphicsDevice;
        private Compositor _compositor;
        private CompositionDrawingSurface _surface;
        private CanvasBitmap _currentFrame;

        Queue<MediaStreamSample> doubleBufferingPool = new Queue<MediaStreamSample>(2);

        // R8G8B8A8 is a 32 bit directx pixel format, so bits per frame would be 32;
        const uint BitsPerPixel = 32;

        public Encoder(GraphicsCaptureItem item, uint sourceWidth, uint sourceHeight)
        {
            _item = item;
            _device = CanvasDevice.GetSharedDevice();
            _isRecording = false;
            _sourceWidth = sourceWidth;
            _sourceHeight = sourceHeight;
            CreateMediaObjects();
        }


        internal IAsyncAction EncodeAsync(IRandomAccessStream stream, uint framesPerSecond)
        {
            uint bitrateInBps = BitsPerPixel * _sourceWidth * _sourceHeight * framesPerSecond / 1024;
            return EncodeInternalAsync(stream, _sourceWidth, _sourceHeight, bitrateInBps, framesPerSecond).AsAsyncAction();
        }

        private async Task EncodeInternalAsync(IRandomAccessStream stream, uint width, uint height, uint bitrateInBps, uint frameRate)
        {
            if (!_isRecording)
            {
                _isRecording = true;

                var encodingProfile = new MediaEncodingProfile();
                encodingProfile.Container.Subtype = "MPEG4";
                encodingProfile.Video.Subtype = "H264";
                encodingProfile.Video.Width = width;
                encodingProfile.Video.Height = height;
                encodingProfile.Video.Bitrate = bitrateInBps;
                encodingProfile.Video.FrameRate.Numerator = frameRate;
                encodingProfile.Video.FrameRate.Denominator = 1;
                encodingProfile.Video.PixelAspectRatio.Numerator = 1;
                encodingProfile.Video.PixelAspectRatio.Denominator = 1;

                StartFrameCapture();

                var transcode = await _transcoder.PrepareMediaStreamSourceTranscodeAsync(_mediaStreamSource, stream, encodingProfile);
                await transcode.TranscodeAsync();
            }
        }

        public void Dispose()
        {
            if (_closed)
            {
                return;
            }
            _closed = true;

            if (!_isRecording)
            {
                DisposeInternal();
            }

            _isRecording = false;
        }

        private void DisposeInternal()
        {

        }

        private void CreateMediaObjects()
        {


            // Describe our input: uncompressed BGRA8 buffers
            var videoProperties = VideoEncodingProperties.CreateUncompressed(MediaEncodingSubtypes.Bgra8, _sourceWidth, _sourceHeight);
            _videoDescriptor = new VideoStreamDescriptor(videoProperties);


            // Create our MediaStreamSource
            _mediaStreamSource = new MediaStreamSource(_videoDescriptor);
            _mediaStreamSource.BufferTime = TimeSpan.FromSeconds(0);
            _mediaStreamSource.Starting += OnMediaStreamSourceStarting;
            _mediaStreamSource.SampleRequested += OnMediaStreamSourceSampleRequested;

            // Create our transcoder
            _transcoder = new MediaTranscoder();
            _transcoder.HardwareAccelerationEnabled = true;

        }

        private void OnMediaStreamSourceSampleRequested(MediaStreamSource sender, MediaStreamSourceSampleRequestedEventArgs args)
        {
            if (_isRecording && !_closed)
            {
                while (doubleBufferingPool.Count == 0)
                {

                }

                lock (doubleBufferingPool)
                {
                    args.Request.Sample = doubleBufferingPool.Dequeue();
                    Debug.WriteLine("Capture");

                }
            }
            else
            {
                args.Request.Sample = null;
                Debug.WriteLine("Dead!");
                StopCapture();
            }
        }


        private void OnMediaStreamSourceStarting(MediaStreamSource sender, MediaStreamSourceStartingEventArgs args)
        {

            while (doubleBufferingPool.Count == 0)
            {

            }
            var sample = doubleBufferingPool.Dequeue();
            TimeSpan timeStamp = sample.Timestamp;
            args.Request.SetActualStartPosition(timeStamp);
        }


        internal void Stop()
        {
            _isRecording = false;
            doubleBufferingPool.Clear();
        }


        internal void StartFrameCapture()
        {
            _lastSize = _item.Size;

            _framePool = Direct3D11CaptureFramePool.Create(
               _device, // D3D device
               DirectXPixelFormat.B8G8R8A8UIntNormalized, // Pixel format
               2, // Number of frames
               _item.Size); // Size of the buffers

            _framePool.FrameArrived += (s, a) =>
            {
                //// The FrameArrived event is raised for every frame on the thread
                //// that created the Direct3D11CaptureFramePool. This means we
                //// don't have to do a null-check here, as we know we're the only
                //// one dequeueing frames in our application.  

                //// NOTE: Disposing the frame retires it and returns  
                //// the buffer to the pool.


                using (var frame = _framePool.TryGetNextFrame())
                {
                    MediaStreamSample sampleToUseLater = MediaStreamSample.CreateFromDirect3D11Surface(frame.Surface, frame.SystemRelativeTime);

                    lock (doubleBufferingPool)
                    {
                        while (doubleBufferingPool.Count >= 2)
                        {
                            // Stops too many samples from being saved
                            doubleBufferingPool.Dequeue();
                        }

                        doubleBufferingPool.Enqueue(sampleToUseLater);
                    }
                }
            };


            _item.Closed += (s, a) =>
            {
                StopCapture();
            };

            _session = _framePool.CreateCaptureSession(_item);

            _session.StartCapture();
        }




        private void ProcessFrame(Direct3D11CaptureFrame frame)
        {
            // Resize and device-lost leverage the same function on the
            // Direct3D11CaptureFramePool. Refactoring it this way avoids
            // throwing in the catch block below (device creation could always
            // fail) along with ensuring that resize completes successfully and
            // isn’t vulnerable to device-lost.
            bool needsReset = false;
            bool recreateDevice = false;

            if ((frame.ContentSize.Width != _lastSize.Width) ||
                (frame.ContentSize.Height != _lastSize.Height))
            {
                needsReset = true;
                _lastSize = frame.ContentSize;
            }

            try
            {
                // Take the D3D11 surface and draw it into a  
                // Composition surface.

                // Convert our D3D11 surface into a Win2D object.
                CanvasBitmap canvasBitmap = CanvasBitmap.CreateFromDirect3D11Surface(
                    _device,
                    frame.Surface);

                _currentFrame = canvasBitmap;

                // Helper that handles the drawing for us.
                FillSurfaceWithBitmap(canvasBitmap);
            }

            // This is the device-lost convention for Win2D.
            catch (Exception e) when (_device.IsDeviceLost(e.HResult))
            {
                // We lost our graphics device. Recreate it and reset
                // our Direct3D11CaptureFramePool.  
                needsReset = true;
                recreateDevice = true;
            }

            if (needsReset)
            {
                ResetFramePool(frame.ContentSize, recreateDevice);
            }
        }

        private void FillSurfaceWithBitmap(CanvasBitmap canvasBitmap)
        {
            CanvasComposition.Resize(_surface, canvasBitmap.Size);

            using (var session = CanvasComposition.CreateDrawingSession(_surface))
            {
                session.Clear(Colors.Transparent);
                session.DrawImage(canvasBitmap);
            }

        }

        private void ResetFramePool(SizeInt32 size, bool recreateDevice)
        {
            do
            {
                try
                {
                    if (recreateDevice)
                    {
                        _device = new CanvasDevice();
                    }

                    _framePool.Recreate(
                        _device,
                        DirectXPixelFormat.B8G8R8A8UIntNormalized,
                        2,
                        size);
                }
                // This is the device-lost convention for Win2D.
                catch (Exception e) when (_device.IsDeviceLost(e.HResult))
                {
                    _device = null;
                    recreateDevice = true;
                }
            } while (_device == null);
        }

        public void StopCapture()
        {
            _session?.Dispose();
            _framePool?.Dispose();
            _item = null;
            _session = null;
            _framePool = null;
        }
    }
}
