﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Composition;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics;
using Windows.Graphics.Capture;
using Windows.Graphics.DirectX;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Media.Imaging;

namespace screenRecorderTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        // Capture API objects.
        private SizeInt32 _lastSize;
        private GraphicsCaptureItem _item;
        private Direct3D11CaptureFramePool _framePool;
        private GraphicsCaptureSession _session;

        // Non-API related members.
        private CanvasDevice _canvasDevice;
        private CompositionGraphicsDevice _compositionGraphicsDevice;
        private Compositor _compositor;
        private CompositionDrawingSurface _surface;
        private CanvasBitmap _currentFrame;
        private string _screenshotFilename = "test.png";


        public MainPage()
        {
            this.InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            FillResolutionDictionary();
            _canvasDevice = new CanvasDevice();

            _compositionGraphicsDevice = CanvasComposition.CreateCompositionGraphicsDevice(
                Window.Current.Compositor,
                _canvasDevice);

            _compositor = Window.Current.Compositor;

            _surface = _compositionGraphicsDevice.CreateDrawingSurface(
                new Size(400, 400),
                DirectXPixelFormat.B8G8R8A8UIntNormalized,
                DirectXAlphaMode.Premultiplied);    // This is the only value that currently works with
                                                    // the composition APIs.

            // You eventually will have an updating surface somewhere here

        }

        public async Task StartCaptureAsync()
        {
            // The GraphicsCapturePicker follows the same pattern the
            // file pickers do.
            var picker = new GraphicsCapturePicker();
            GraphicsCaptureItem item = await picker.PickSingleItemAsync();

            // The item may be null if the user dismissed the
            // control without making a selection or hit Cancel.
            if (item != null)
            {
                await StartCaptureInternal(item);
            }
        }

        private async Task StartCaptureInternal(GraphicsCaptureItem item)
        {
            
            hasTimerStopped = false;

            bool isDifferentFromNativeResolution = false;
            framesPerSecond = FPSList[FPSComboBox.SelectedIndex];


            if (ResolutionComboBox.SelectedIndex != 0)
            {
                isDifferentFromNativeResolution = true;
                resolutionSize = DetermineResolutionSize();
                _encoder = new Encoder(item,(uint)resolutionSize.Width, (uint)resolutionSize.Height);
            }
            else
            {
                _encoder = new Encoder(item,(uint)item.Size.Width, (uint)item.Size.Height);
            }
            intervalTime = TimeSpan.FromMilliseconds(1d / framesPerSecond * MillisecondsInSeconds);
            RecordButton.IsEnabled = false;
            StopButton.IsEnabled = true;

            StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;
            StorageFile tempFile = await tempFolder.CreateFileAsync("temp.mp4", CreationCollisionOption.ReplaceExisting);

            using (var stream = await tempFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                await _encoder.EncodeAsync(
                    stream,
                    framesPerSecond);
            }
            
        }

       


        private void StopRecording()
        {
            hasTimerStopped = true;
            StopButton.IsEnabled = false;
            RecordButton.IsEnabled = true;
            _encoder.Stop();

        }

   

        private async void ScreenshotButton_ClickAsync(object sender, RoutedEventArgs e)
        {
            await SaveImageAsync(_screenshotFilename, _currentFrame);
        }

        private async Task SaveImageAsync(string filename, CanvasBitmap frame)
        {
            StorageFolder pictureFolder = KnownFolders.SavedPictures;

            StorageFile file = await pictureFolder.CreateFileAsync(
                filename,
                CreationCollisionOption.ReplaceExisting);

            using (var fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                await frame.SaveAsync(fileStream, CanvasBitmapFileFormat.Png, 1f);
            }
        }


        Encoder _encoder;
        Stopwatch stopwatch = new Stopwatch();
        
        Size resolutionSize = new Size(1, 1);
        //CompositionExportHelper exportHelper = new CompositionExportHelper();
        const uint MillisecondsInSeconds = 1000;
        uint framesPerSecond = 15;
        List<uint> FPSList = new List<uint> { 15, 30, 60 };
        Dictionary<string, Size> resolutionDictionary = new Dictionary<string, Size>();

        List<string> screenResolutionList = new List<string>()
        {
           "Native Resolution",
           "1080P",
           "720P",
           "480P",
           "360P",
        };


        TimeSpan intervalTime;



        bool hasTimerStopped = false;

        

        
        private void FillResolutionDictionary()
        {
            resolutionDictionary.Add("1080P", new Size(1920, 1080));
            resolutionDictionary.Add("720P", new Size(1280, 720));
            resolutionDictionary.Add("480P", new Size(854, 480));
            resolutionDictionary.Add("360P", new Size(640, 360));
        }

       
        private async void RecordButton_Click(object sender, RoutedEventArgs e)
        {
            await StartCaptureAsync();
        }

        private Size DetermineResolutionSize()
        {
            string key = screenResolutionList[ResolutionComboBox.SelectedIndex];
            Size resolutionValue = resolutionDictionary[key];

            // Easy way of determining if in window is portrait
            if (Window.Current.Bounds.Height > Window.Current.Bounds.Width)
            {
                Size portraitResolutionValue = new Size(resolutionValue.Height, resolutionValue.Width);
                return portraitResolutionValue;
            }

            return resolutionValue;
        }

        
        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            StopRecording();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            FPSComboBox.SelectedIndex = 0;
            ResolutionComboBox.SelectedIndex = 0;
        }
    }
}
