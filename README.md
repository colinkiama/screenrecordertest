# UWP Screen Recorder Test

## Description
This demo uses the UWP Screen Capture APIs to record a screen capture directly to a file. (Each frame is saved to the video as they are received)

## Purpose
This was made in order to test the screen capture APIs

## Special Thanks
This wouldn't have been possible without the SimpleRecorder source code!: https://github.com/robmikh/SimpleRecorder


